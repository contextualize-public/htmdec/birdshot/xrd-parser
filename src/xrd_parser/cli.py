#!/usr/bin/env python

import argparse
import logging
import numpy as np
import os
import pandas as pd
import re
import sys

from functools import partial, cmp_to_key
from pathlib import Path

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(os.environ.get("LOGLEVEL", logging.INFO))


def reduce(df: pd.DataFrame, method: str) -> pd.DataFrame:
    """
    Reduce along the columns of the dataframe.

    Parameters
    ----------
    args : argparse namespace
        Options parsed from the command line.
    df : pandas.DataFrame
        Dataframe to be reduced.
    method : str
        Method to apply as the reduction. Any reduction method
        recognized by numpy may be specified.
    
    Results
    -------
    pandas.DataFame
    """
    def number_aware_label(*labels):
        def try_numeric(x):
            try:
                return int(x)
            except ValueError:
                pass
            try:
                return float(x)
            except ValueError:
                pass
            return x
        
        # This pattern will also match '.##.##.##' as a single number.
        # However, that will not successfully convert to an int or float.
        # Therefore, the result above would compare lexicographically.
        pattern = r'([-+]*\d*\.*\d+\.*\d*)'
        words = [re.split(pattern, label) for label in labels]
        label = []
        fill = f"({method.title()})"
        for word in zip(*words):
            if all([(w == word[0]) for w in word]):
                label.append(word[0])
            else:
                if label[-1] != fill:
                    label.append(fill)
        return ''.join(label)
        
    reduction = partial(getattr(np, method), axis=1)
    columns = df.columns
    df[number_aware_label(*columns)] = reduction(df)
    return df.drop(columns=columns)


def number_aware_sort(lhs, rhs):
    """
    Sorts two strings that may, or may not, contain numbers. If they do
    include numbers, sort those numbers numerically rather than
    lexicographically.
    """
    def try_numeric(x):
        try:
            return int(x)
        except ValueError:
            pass
        try:
            return float(x)
        except ValueError:
            pass
        return x
    
    # This pattern will also match '.##.##.##' as a single number.
    # However, that will not successfully convert to an int or float.
    # Therefore, the result above would compare lexicographically.
    pattern = r'([-+]*\d*\.*\d+\.*\d*)'
    lwords = re.split(pattern, lhs)
    rwords = re.split(pattern, rhs)
    for lword, rword in zip(lwords, rwords):
        l = try_numeric(lword)
        r = try_numeric(rword)
        if l < r:
            return -1
        elif r < l:
            return 1
    if len(lhs) < len(rhs):
        return -1
    elif len(rhs) < len(lhs):
        return 1
    return 0


def list_files(path, *, recursive: bool=False):
    """
    List files in the path. If path is a file, simply return the file
    (as a list). If path is a directory, return a listing of all files in that
    directory.

    Parameters
    ----------
    path : str | path-like
        File or directory from which to construct the file listing.
    recursive : bool (optional)
        If True and path is a directory, list files recursively.

    Returns
    -------
    list[Path]
        List of pathlib.Path entries.
    """
    if path.is_dir():
        if recursive:
            for (dirpath, dirnames, filenames) in os.walk(p):
                files = [Path.joinpath(dirpath, fname) for fname in filenames]
        else:
            files = [Path(fname) for fname in Path(path).glob('*') if fname.is_file()]
    else:
        files = [Path(path)]
    logger.debug("Processing:\n" + "\n\t".join(map(str, files)))
    return files


class BirdshotAntonPaar:
    def __init__(self):
        self._filename_pattern__ = re.compile(r'Sample (?P<sample>\d+) Omega (?P<omega>[-+0-9]+)d? Coupled.*2T (?P<start>[0-9.]+)?. - (?P<end>[0-9.]+)?. ContHCR ?(?P<suffix>[ a-zA-Z0-9_]+)?\.scn')

    @property
    def filename_pattern(self):
        return self._filename_pattern__

    def composite(self, args) -> pd.DataFrame:
        """
        Composite files based on the options in `args`.

        Parameters
        ----------
        args : argparse.ArgumentParser namespace
            Arguments processed from the command line parameters.

        Returns
        -------
        None
        """
        pattern = self.filename_pattern
        files = [
            f for p in args.paths for f in list_files(p, recursive=args.recursive)
            if re.match(pattern, f.name)
        ]
        logger.info(f"Processing {len(files)} files.")
        combined = None
        for file in files:
            match = re.match(pattern, file.name)
            omega = int(match.group("omega"))
            sample = int(match.group("sample"))
            if args.omega is not None and omega != args.omega:
                logger.debug(f"Skipping {file!r}. Omega ({omega} != {args.omega}) do not match.")
                continue
            if args.sample is not None and sample != args.sample:
                logger.debug(f"Skipping {file!r}. Samples ({sample} != {args.sample}) do not match.")
                continue
            logger.debug(f"Processing {file!r}.")
            df = pd.read_csv(file, sep="\t", header=None)
            df.columns = ["Angle", f"Sample {sample}::Omega {omega:+d}d".strip()]
            if combined is None:
                combined = df
            else:
                if len(df) != len(combined):
                    raise ValueError(f"{file!r} is the same size as other measurements and cannot be combined.")
                # TODO: Add a domain check.
                series = df.iloc[:, 1]
                combined[series.name] = series
        combined.set_index("Angle", drop=True, inplace=True)
        columns = sorted(combined.columns, key=cmp_to_key(number_aware_sort))
        return combined[columns]


def parse_args(argv):
    parser = argparse.ArgumentParser(
        description="Composite XRD scans. File names are assumed to be of the form " \
                    "'Sample # Omega +/-#d Coupled Scan OmO 0.0° 2T ##.#° - ##.#° ContHCR MB[ #].scn' " \
                    "that provides sample ID, omega angle, start and end angle " \
                    "and optional measurement index, respectively."
    )
    parser.add_argument(
        "paths",
        nargs="*",
        type=Path,
        default=Path("."),
        help="One or more files or a directory that contains XRD files with " \
             "the matching filename pattern. Default: '.'"
    )
    parser.add_argument(
        "--header",
        dest="header",
        action="store_true",
        default=True,
        help="Print a header row (column labels) in the output file."
    )
    parser.add_argument(
        "--no-header",
        dest="header",
        action="store_false",
        help="Do not print a header row (column labels) in the output file."
    )
    parser.add_argument(
        "--omega",
        default=None,
        type=int,
        help="Specify what omega angle to be composited. Default: All, if sample is specified; 0, otherwise."
    )
    parser.add_argument(
        "-o", "--output",
        type=Path,
        default=Path("output.csv"),
        help="Name of the output filename. Default: 'output.csv'"
    )
    parser.add_argument(
        "-r", "--reduce",
        metavar="METHOD",
        default=None,
        help="Apply an optional reduction along the composited columns. Any "
             "reduction method from the 'numpy' library may be given, e.g. "
             "mean, median, mode, std. The original columns are dropped."
    )
    parser.add_argument(
        "-R", "--recursive",
        action="store_true",
        default=False,
        help="Search directories recursively. Default: False."
    )
    parser.add_argument(
        "--sample",
        default=None,
        type=int,
        help="Specify what sample is to be composited. Default: All, if omega is specified."
    )
    args = parser.parse_args(argv)
    # Handle interdependent options
    if (args.sample, args.omega) == (None, None):
        args.sample, args.omega = (None, 0)
    # Done
    return args


def run(argv):
    args = parse_args(argv)
    df = BirdshotAntonPaar().composite(args)
    if args.reduce:
        df = reduce(df, args.reduce)
    try:
        writer = {
            ".csv": partial(df.to_csv, args.output, sep=",", header=args.header)
        }[args.output.suffix.lower()]
        writer()
    except KeyError:
        raise IOError(f"{df.suffix!r} is not a recognized file format.")


def main():
    try:
        run(sys.argv[1:])
    except Exception as e:
        logger.error(f"An unexpected error occurred, {str(e)!r}")
        sys.exit(1)


if __name__ == "__main__":
    main()
