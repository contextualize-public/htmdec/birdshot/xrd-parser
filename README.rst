.. These are examples of badges you might want to add to your README:
   please update the URLs accordingly

    .. image:: https://api.cirrus-ci.com/github/<USER>/xrd-parser.svg?branch=main
        :alt: Built Status
        :target: https://cirrus-ci.com/github/<USER>/xrd-parser
    .. image:: https://readthedocs.org/projects/xrd-parser/badge/?version=latest
        :alt: ReadTheDocs
        :target: https://xrd-parser.readthedocs.io/en/stable/
    .. image:: https://img.shields.io/coveralls/github/<USER>/xrd-parser/main.svg
        :alt: Coveralls
        :target: https://coveralls.io/r/<USER>/xrd-parser
    .. image:: https://img.shields.io/pypi/v/xrd-parser.svg
        :alt: PyPI-Server
        :target: https://pypi.org/project/xrd-parser/
    .. image:: https://img.shields.io/conda/vn/conda-forge/xrd-parser.svg
        :alt: Conda-Forge
        :target: https://anaconda.org/conda-forge/xrd-parser
    .. image:: https://pepy.tech/badge/xrd-parser/month
        :alt: Monthly Downloads
        :target: https://pepy.tech/project/xrd-parser
    .. image:: https://img.shields.io/twitter/url/http/shields.io.svg?style=social&label=Twitter
        :alt: Twitter
        :target: https://twitter.com/xrd-parser

.. image:: https://img.shields.io/badge/-PyScaffold-005CA0?logo=pyscaffold
    :alt: Project generated with PyScaffold
    :target: https://pyscaffold.org/

|

==========
xrd-parser
==========


    Add a short description here!


A longer description of your project goes here...


.. _pyscaffold-notes:

Note
====

This project has been set up using PyScaffold 4.5. For details and usage
information on PyScaffold see https://pyscaffold.org/.
